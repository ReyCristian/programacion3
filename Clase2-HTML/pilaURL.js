
const file = require ("./tools/file.js");
const TableLector = (require ("./tools/table.js")).TableLector
const ProductTableLector = (require ("./tools/productTable.js")).ProductTableLector


const usuarioController= new (require("./tools/UsuarioController.js").Controller)();

function cargar(server) {
  server.usuarioController=usuarioController;
  server.get('/style.css', (request, response)=>{
    response.setHeader("Content-Type","text/css");
      file.leer('html/style.css',response,(data)=>data);
  })

  server.usuarioController=usuarioController;
  server.get('/style.js', (request, response)=>{
    response.setHeader("Content-Type","application/javascript");
      file.leer('html/style.js',response,(data)=>data);
  })

  server.use(function(request, response, next) {
    request.usuario=usuarioController.getUsuario(request.cookies,response);
    console.log(request.usuario);
    console.log(request.url);
    next();

  });

  server.get('/', (request, response)=>{
      file.leer('html/index.html',response,(data)=>data);
  });

  server.get('/salir', (request, response)=>{
      response.clearCookie("user");
      file.leer('html/index.html',response,(data)=>data);
  });

  new ProductTableLector(server);

  new TableLector(server,'servicios',['codigo','nombre','precio']);

  new TableLector(server,'tablaNombres',['nombre','apellido','edad']);

  server.get('/perfil', (request, response)=>{
    file.leer('html/perfil.html',response,(data)=>data);
  });

  server.get('/cached', (request, response)=>{
    file.leer('html/tools/cache.html',response,(data)=>data);
  });


  server.use(function(req, res, next) {
    file.leer('html/404.html',res.status(404),(data)=>data);
  });



}


exports.cargar=cargar;
