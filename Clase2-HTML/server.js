//inicia el npm
//$ npm init

//instalamos el express
//$ npm install express --save
const express = require("express");
const server = express();

//este modulo viene incluido y maneja los datos POST y GET
const bodyParser = require('body-parser');
server.use(bodyParser.urlencoded({ extended: false }));

//este modulo tambien se instala
//npm install cookie-parser --save
var cookieParser = require('cookie-parser');
server.use(cookieParser());

//este modulo contiene las url a las que tendra acceso el usuario
let pila = require("./pilaURL.js");

//permitimos selecconar el puerto al llamar al puerto_server
//$ nodejs server.js 80
var puerto_server = 8080;
var p = parseInt(process.argv[2]);
if (!isNaN(p))
  puerto_server = p;

//inicia el servidor
server.listen(puerto_server, () => {
 console.log("El servidor está inicializado en el puerto "+puerto_server);
});

server.use(express.static('public'));

server.use("/cached/html3",express.static('../Clase3-HTML'));
server.use("/cached/html",express.static('../Clase4-HTML'));
server.use("/cached/bootstrap",express.static('../Clase4-bootstrap'));





pila.cargar(server);
