
const Usuario = (require("../models/Usuario.js")).Class;
const file = require ("./file.js");

const fileName = "usuarios.json";


exports.Controller= class UsuarioController {
  constructor() {
    this.usuarios = [];
    try{
      this.usuarios = file.readJson(fileName);
    }
    catch(e){
      console.error(e);
    }

  }

  getUsuario(cookies,response){
    var usuario;
    if (cookies!=null){
      var user = cookies.user;
      if (user!=null)
        if (user.id!=null)
          usuario = this.usuarios[user.id];
          if (usuario!=null)
          if (usuario.sessionPass==user.pass)
            return usuario;
    }
    usuario = this.createUsuario();
    response.cookie("user" ,({"id":usuario.id,"pass":usuario.sessionPass}));
    return usuario;
  }

  createUsuario(){
    var nuevo = new Usuario(this.usuarios.length);
    this.usuarios.push(nuevo);
    file.writeJson(fileName,this.usuarios);
    return nuevo;
  }
}
