
//este modulo maneja archivos
let fs = require('fs');


//funcion para leer paginas html y enviarlas
//permite modificar la pagina antes de enviar
//por ejemplo si en la pagina incluimos un texto que contenga "{titulo}"
//este puede ser remplazado con data.replace
//  leer('html/404.html',res.status(404),(data)=>data.replace("{titulo}","404"));
function leer(archivo,response,funcion){
  fs.readFile(archivo, 'utf-8', (err, data) => {
    if(err) {
      console.log('error: ', err);
        response.send('[GET]Error al cargar');
    } else {
        response.send(funcion(data));
    }
  });
}

//funcion para leer archivos JSON
function readJson(nombreArchivo){
  try{
  var archivo = fs.readFileSync("json/"+nombreArchivo);

  return JSON.parse(archivo);
  }
  catch(error){
    console.error(error);
    fs.mkdir("json/",function(e){
      if(!e || (e && e.code === 'EEXIST')){
      } else {
      }
    });

    return [];
  }
}
//funcion para guardar archivos JSON
function writeJson(nombreArchivo,json){
  fs.writeFile("json/"+nombreArchivo,JSON.stringify(json),function (err) {
    if (err) {
        return console.log(err);
    }
    console.log("Json guardado:");
    console.log(json);
    }
  );
}
function add(nombreArchivo){
  var archivo = fs.readFileSync(nombreArchivo);
  return archivo;
}


exports.leer = leer;
exports.readJson = readJson;
exports.writeJson = writeJson;
exports.add=add;
