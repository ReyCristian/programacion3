const TableLector = (require ("./table.js")).TableLector;
const file = require ("./file.js");

class ProductTableLector extends TableLector{
  constructor(server,nombre){
    super(server,'productos',['codigo','nombre','marca','precio']);

    server.get('/producto.html', (request, response)=>{
      file.leer('html/detallesProducto.html',response,(data)=>{
          return data.replace("{imagen}","images/"+request.query.get+".png");
      });
    });
  }
  addRegistro(data,obj){

     data+= '<tr>'
     for (var celda of this.encabezado){
       if (celda=='nombre'){
         data+='<td id = "celdaProducto'+obj['codigo']+'" ><a href="javascript:mostrar(\''+obj['codigo']+'\')">'+obj[celda]+'</a></td>';
       }
       else
        data+='<td>'+obj[celda]+'</td>';
     }
     return data;
 }
 addFunciones(data){
   return data+'<script src="js/DetailLector.js"></script>';
 }
}


exports.ProductTableLector=ProductTableLector;
