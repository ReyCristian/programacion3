const file = require ("./file.js");

class TableLector {
  constructor(server,nombre,encabezado){
    this.encabezado = encabezado;
    this.nombre = nombre;

    server.get('/'+nombre, (request, response)=>{
        file.leer('html/'+nombre+'.html',response,(data)=>{
          var r = data.replace("{datos_tabla}",this.tablaDatos(file.readJson(nombre+".json"),request.query.admin));
          if(request.query.admin){
            r = r.replace("{formulario_anexacion}",this.addDataLoader());
            r+=file.add('html/tools/DeleteRowTable.html');
          }
          else
            r = r.replace("{formulario_anexacion}",file.add('html/tools/botoneditar.html'));
            r = this.addFunciones(r);
          return r;
        }
            );
    })

    server.post('/'+nombre,(request, response)=>{
      file.leer('html/'+nombre+'.html',response,(data)=>{
        var tabla = file.readJson(nombre+".json");

        if (request.body.delete!=null){
          try{
            tabla.splice(parseInt(request.body.delete),1);
          }
          catch (error){
            console.error(error);
          }
        }
        else{
          tabla.push(request.body);
        }

        file.writeJson(nombre+".json",tabla);

        var r=data
          .replace("{datos_tabla}",this.tablaDatos(tabla,true))
          .replace("{formulario_anexacion}",this.addDataLoader());

        r+=file.add('html/tools/DeleteRowTable.html');
        return r;

        }
      );
    })
  }
  addDataLoader(){
    var data="";
    data += '<form action="/'+this.nombre+'" method="post"><table>';

    for (var columna in this.encabezado){
      data += '<tr><td>'+this.encabezado[columna]+':   </td>  <td><input type="text" name="'+this.encabezado[columna]+'" value="">   </td>';
    }
    data +='</tr><tr><td></td><td><button type="submit">Agregar</button>       </td></table></form>';
    return data;
  }
  //funcion para convertir un arreglo json en una tabla html
  //(no incluye header,aun)
  tablaDatos(json,deleteable){
    try{

    var data = "";
    var id = 0;
    for (var obj of json){
    data = this.addRegistro(data,obj);
      if (deleteable)
        data += '<td><a href="javascript:borrar('+id+')">'+file.add('html/tools/botonborrar.html')+'</a></td>';
      data += '</tr>'
      id++;
      }
      return data;
    }
    catch(error)
    {
      console.error(error);
      return "";
    }
  }
  addRegistro(data,obj){

     data+= '<tr>'
     for (var celda of this.encabezado){
       data+='<td>'+obj[celda]+'</td>';
     }
     return data;
 }
  addFunciones(data){
    return data;
  }
}



exports.TableLector = TableLector;
