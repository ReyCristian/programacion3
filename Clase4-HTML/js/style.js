var iniciando=true

function abrirPagina(pagina){
  if(iniciando){
    inicio();
  }
  iniciando=false;
  document.getElementById('framePagina').src=pagina+".html";
  escribir(pagina)
}


function mover(item,alturaInicio,alturaFin,avanceInicio,avanceFin,delay) {
  if (alturaInicio>alturaFin)
    alturaInicio-=1;
  if (avanceInicio<avanceFin)
      avanceInicio+=5;

  item.style.top=alturaInicio+"px";
  item.style.left=avanceInicio+"px";
  if(alturaInicio>alturaFin || avanceInicio<avanceFin)
    window.setTimeout(mover,delay,item,alturaInicio,alturaFin,avanceInicio,avanceFin,delay);
}


function inicio(){
    document.getElementById('lema').style.display='none';
    document.getElementById('encabezado').style.height='150px';


    document.getElementById('botonProductos').className="botonBarra";

    var botonServicios=document.getElementById('botonServicios');
    botonServicios.className="botonBarra";
    mover(botonServicios,30,0,0,140,1);

    var botonPerfil=document.getElementById('botonPerfil');
    botonPerfil.className="botonBarra";
    mover(botonPerfil,60,0,0,260,15);

    var botonera = document.getElementById('barraBotones');
    botonera.style.position='absolute';
    mover(botonera,219,110,0,0,3);

    document.getElementById('paginaActual').style.height="68%";
    document.getElementById('encabezado').style.height="160px";
  }

var tipiando=null;
function escribir(titulo){

  document.title=titulo;
  if (tipiando!=null)
    clearTimeout(tipiando);
  tipiar(document.getElementById('bienvenida'),titulo);
}

function tipiar(hoja,texto){
  if (hoja.innerHTML.length==1)
    hoja.innerHTML=texto.substring(0,1);
  if (hoja.innerHTML==texto.substring(0,hoja.innerHTML.length))
    hoja.innerHTML=texto.substring(0,hoja.innerHTML.length+1);
  else
    hoja.innerHTML=hoja.innerHTML.substring(0,hoja.innerHTML.length-1);
  if (hoja.innerHTML!=texto)
    tipiando=setTimeout(tipiar,100,hoja,texto);
}
