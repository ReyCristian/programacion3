var iniciando=true

function abrirPagina(pagina){
  if(iniciando){
    inicio();
  }
  iniciando=false;
  document.getElementById('framePagina').src=pagina+".html";
  escribir(pagina)
}


function mover(item,alturaInicio,alturaFin,avanceInicio,avanceFin,delay) {
  if (alturaInicio>alturaFin)
    alturaInicio-=1;
  if (avanceInicio<avanceFin)
      avanceInicio+=5;

  item.style.top=alturaInicio+"px";
  item.style.left=avanceInicio+"px";
  if(alturaInicio>alturaFin || avanceInicio<avanceFin)
    window.setTimeout(mover,delay,item,alturaInicio,alturaFin,avanceInicio,avanceFin,delay);

}

function encojerLema(altura){
	
	if (altura>0){
		altura--;
		window.setTimeout(encojerLema,3,altura);
	}
	document.getElementById('lema').style.height=altura+"px";
	
}



function inicio(){
    //document.getElementById('lema').style.display='none';
	document.getElementById('lema').style.color=window.getComputedStyle( document.getElementById("encabezado") ,null).getPropertyValue('background-color');
    //document.getElementById('encabezado').style.height='150px';
	
	encojerLema(parseInt(window.getComputedStyle( document.getElementById("lema") ,null).getPropertyValue('height').replace(/px/,"")));

	//document.getElementById('botonProductos').className="botonBarra";

    var botonServicios=document.getElementById('botonServicios');
    //botonServicios.className="botonBarra";
    mover(botonServicios,50,0,0,100,15);

    var botonPerfil=document.getElementById('botonPerfil');
	//botonPerfil.className="botonBarra";
    mover(botonPerfil,100,0,0,200,10);

    var botonera = document.getElementById('barraBotones');
    botonera.style.position='absolute';
    mover(botonera,50,0,0,0,3);

    document.getElementById('paginaActual').style.height="68%";
    document.getElementById('encabezado').style.height="30%";
	
	window.setTimeout(()=>{
		
		var barra =document.getElementById('barraBotones');
		barra.className="btn-group";
		
		botonServicios.style.left="0px";
		botonPerfil.style.left="0px";
		},2000)
  }

var tipiando=null;
function escribir(titulo){

  document.title=titulo;
  if (tipiando!=null)
    clearTimeout(tipiando);
  tipiar(document.getElementById('bienvenida'),titulo);
}

function tipiar(hoja,texto){
  if (hoja.innerHTML.length==1)
    hoja.innerHTML=texto.substring(0,1);
  if (hoja.innerHTML==texto.substring(0,hoja.innerHTML.length))
    hoja.innerHTML=texto.substring(0,hoja.innerHTML.length+1);
  else
    hoja.innerHTML=hoja.innerHTML.substring(0,hoja.innerHTML.length-1);
  if (hoja.innerHTML!=texto)
    tipiando=setTimeout(tipiar,100,hoja,texto);
}
