class Display{

  constructor(id){
    this.display = document.getElementById(id);
    this.prefijo = "";

  }

  mostrar(){
    var display = `<table class="table table-hover table-striped">
                    <thead>
                      <tr class="table-primary">
                       ${this.getHead()}
                      </tr>
                    </thead>
                      <tbody>
                        ${this.getRows(this.lista.get())}
                      <tbody>`


    this.display.innerHTML = display;

  }

  getHead(){
    throw 'Display es "abstracta" necesita implementar "getHead()"';
  }


  getRows(items){
      var i = 0
      var r ="";
      for (var item of items) {
        r +=  `<tr class="clickable-row" data-toggle="collapse" data-target="#itemDetail${this.prefijo}${i}" aria-controls="itemDetail${this.prefijo}${i}" aria-expanded="false">
                  ${this.getRow(item)}
              </tr>
              <tr class="clickable-row collapse" id="itemDetail${this.prefijo}${i++}">
                <th colspan=4>${this.getDetails(item)}</th>
              </tr>`;
      }
      return r;

  }

  getRow(){
    throw 'Display es "abstracta"  necesita implementar "getRow()';
  }
  getDetails(){
    throw 'Display es "abstracta"  necesita implementar "getDetails()';
  }
  getFile(name){
    //   const display = this.display;
    //   $.ajax
    //   (
    //   	{
    //       	url : "../data/"+name,
    // 		    dataType: "text",
    // 		    success : function (data)
    //         {
    //              display.innerHTML="<pre>"+data+"</pre>";
    // 		    }
    // 	  }
    // );
    //
    var r=   $("#"+name).html();
    return r;
  }

  add(item){
    item.prefijo=this.prefijo;
    this.lista.get().push(item);
    this.mostrar();
  }
  del(item){
    var i  = this.lista.get().findIndex(e => (e.id==item.id));
    i !== -1 && this.lista.get().splice( i, 1 );
    this.mostrar()
  }

  getNextID(){
    return this.lista.get()[this.lista.get().length- 1].id+1;
  }
  setPrefijo(pref){
    this.prefijo=pref;

    for (var item of this.lista.get())
      item.prefijo = this.prefijo;
  }
}

function redondear(num){
  return (num = Math.round(num * 100) / 100);
}
