class DisplayCarrito extends Display{
  constructor(id){
    super(id)
    this.lista = new ManejadorJson(this.getFile("carrito"));
    this.mostrar();
    this.tools = (item)=>{
      return "";
    }
  }
  getHead(){
    return   `<th scope="col">Nombre</th>
                <th scope="col">Prec. Unit.</th>
                <th scope="col">Cantidad</th>
                <th scope="col">Total</th>
                <th scope="col"></th>`;
  }

  getRow(item){
          var subtotal = redondear(item.item.precio * item.cantidad);
          this.total +=subtotal;

          return `<th scope="row">${item.item.nombre}</th>
                  <td>$${item.item.precio}</td>
                  <td>${item.cantidad}</td>
                  <td>$${subtotal}</td>
                  <td>${this.tools(item)}</td>`;
        }
  getRows(items){
      this.total = 0;
      var r =super.getRows(items);
      this.total=redondear(this.total);
      r +=  `<tr>
              <th scope="row">Total:</th>
              <td></td>
              <td></td>
              <td>$${this.total}</td>
              <td></td>
            </tr>`
      return r;

  }

  getDetails(item){
      // if (item.item.prefijo=="P-")
      //   return this.getDetailsof["P-"](item);
      // if (item.item.prefijo=="S-")
      //   return this.getDetailsof["S-"](item);
      // return "";
        return this.getDetailsof[item.item.prefijo](item.item);
        }

  add(item,cantidad){
    item.id=`${item.prefijo}${item.id}`;
    var encontrado = this.lista.get().find(e => (e.item.id==item.id));
    if (encontrado !=null)
      encontrado.cantidad = parseInt(encontrado.cantidad) + parseInt(cantidad);
    else
      this.lista.get().push({"item":item,"cantidad":cantidad});
    this.mostrar();
  }

  del(item){
    var i  = this.lista.get().findIndex(e => (e.item.id==item.item.id));
    i !== -1 && this.lista.get().splice( i, 1 );
    this.mostrar()
  }
}
