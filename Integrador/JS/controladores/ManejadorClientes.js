class DisplayClientes extends Display{
  constructor(id){
    super(id)

    this.lista = new ManejadorJson(this.getFile("clientes"));
    this.tools = (cliente)=>{
      return "";
    }


    this.mostrar();
    $('#listaClientes > table').on('click', '.clickable-row', function(event) {
      $(this).addClass('table-success').siblings().removeClass('table-success');
    });

  }
  getHead(){
    return   `<th scope="col">Cuil</th>
                <th scope="col">Razon Social</th>
                <th scope="col">Telefono</th>
                <th scope="col">Direccion</th>
                <th scope="col"></th>`;
  }

  getRow(cliente){
          return `<th scope="row">${cliente.cuil}</th>
                  <td>${cliente.razonSocial}</td>
                  <td>${cliente.telefono}</td>
                  <td>${cliente.direccion}</td>
                  <td>${this.tools(cliente)}</td>`
        }
  getRows(items){
      var r ="";
      for (var item of items) {
        r +=  `<tr class="clickable-row">
                  ${this.getRow(item)}
              </tr>`;
      }
      return r;

  }
}
