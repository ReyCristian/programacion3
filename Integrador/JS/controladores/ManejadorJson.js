
class ManejadorJson {
  constructor(jsonText) {
    this.json=JSON.parse(jsonText);
  }

  get(){
    return this.json;
  }

  getSorterJSON(key, orden="asc") {
    return this.get().sort(function (a, b) {
        var x = a[key],
        y = b[key];

        if (orden === 'asc') {
            return ((x < y) ? -1 : ((x > y) ? 1 : 0));
        }

        if (orden === 'desc') {
            return ((x > y) ? -1 : ((x < y) ? 1 : 0));
        }
    });
}
}
