class DisplayProductos extends Display{
  constructor(id){
    super(id)

    this.lista = new ManejadorJson(this.getFile("productos"));
    this.tools = (producto)=>{
      return "";
    }
    this.setPrefijo("P-");
    this.mostrar();

  }
  getHead(){
    return   `<th scope="col">ID</th>
                <th scope="col">Nombre</th>
                <th scope="col">Precio</th>
                <th scope="col"></th>`;
  }

  getRow(producto){
          return `<th scope="row">${producto.id}</th>
                  <td>${producto.nombre}</td>
                  <td>$${producto.precio}</td>
                  <td>${this.tools(producto)}</td>`
        }

  getDetails(producto){
          return `
          <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
              <img class="col-12" src="${producto.imagen}"><br>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
              <h5 class="titulo-detalle">Marca:</h5>${producto.marca}<br>
              <h5 class="titulo-detalle">Modelo:</h5>${producto.modelo}<br>
              <h5 class="titulo-detalle">Caracteristicas:</h5>${producto.caracteristicas}<br>
            </div>
          </div>
          `;
        }


}
