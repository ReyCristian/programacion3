class DisplayServicios extends Display{
  constructor(id){
    super(id)

    this.lista = new ManejadorJson(this.getFile("servicios"));
    this.tools = (servicio)=>{
      return ""
    }
    this.setPrefijo("S-");
    this.mostrar();

  }
  getHead(){
    return   `<th scope="col">ID</th>
                <th scope="col">Nombre</th>
                <th scope="col">Precio x Hora</th>
                <th scope="col"></th>`;
  }

  getRow(servicio){
          return `<th scope="row">${servicio.id}</th>
                  <td>${servicio.nombre}</td>
                  <td>$${servicio.precio}</td>
                  <td>${this.tools(servicio)}</td>`
        }
  getDetails(servicio){
          return `
          <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
              <img class="col-12" src="${servicio.imagen}"><br>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
              <h5 class="titulo-detalle">Descripcion:</h5>${servicio.descripcion}<br>
            </div>
          </div>
          `;
        }

}
