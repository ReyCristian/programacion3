# GIT desde cero

1. **¿Qué es git?**

  Es un *sistema de control de versiones*.

2. **¿Por qué queremos utilizar git?**

  - Llevar un *seguimiento* de los cambios que se van haciendo en nuestros proyectos.
  - *Compartir* código.
  - Trabajar en *equipo* sobre el mismo código.
  - *Recuperar* código que fue modificado.
3. **¿Qué es el bash que instala git?**

  Es una *consola* con funciones agregadas, trae conceptos de Unix.
4. **Describa los estados (working directory, staging area, repository)**

  - *Working directory*: Donde trabajamos con los archivos
  - *Staging area*: Se agregan los archivo para su posterior guardado.
  - *Repository*: Guardado en GIT

5. **Describa el flujo para crear un nuevo repositorio git.**

~~~
git init
git add .
git commit -m "Creando repositorio"
~~~

  En caso de no haber configurado el email, se puede usar:

~~~
git config --global user.name "Rey Cristian"
git config --global user.email "rey.cristian.eze@gmail.com"
~~~

6. **Describa el flujo para agregar un archivo simple al repositorio.**

~~~
git add archivo.simple
git commit -m "Agregando un archivo simple"
~~~

7. **Describa el flujo para cambiar el archivo agregado y guardar los cambios en el repositorio.**

~~~
git add archivo.simple
git commit -m "Modificando un archivo simple"
~~~
8. **¿Cómo hago para ignorar un archivo o carpeta?**

  Lo agregamos al archivo `.gitignore`

9. **Explique qué es un branch. Dé un pequeño ejemplo de cómo haría uno.
Es una *rama* del proyecto. Permite separarse del desarrollo principal y continuar trabajando sin alterar este.**

Lo usaría por ejemplo para tener una *version alternativa* para hacer pruebas mientras tengo el código de la version estable en el master

10. **¿Qué hago con `git add .`?**

Agrega los archivos de la *carpeta actual* (`.`) y sus sub-carpetas al Staging area. Si nos encontramos en el directorio raíz del proyecto, agrega todo.

11. **¿Cómo cambiamos de un branch a otro?**

`git checkout nombrebranch`

12. **Investigue qué es Markdown y responda todas las preguntas anteriores en este lenguaje (con el nombre de archivo RESPUESTAS.md). Súbalo al repositorio.**

https://gitlab.com/ReyCristian/programacion3.git
