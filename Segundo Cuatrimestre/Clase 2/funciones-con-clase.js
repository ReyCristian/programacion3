


class Persona{
  constructor(nombre="NN",edad=0){
    this.nombre = nombre;
    this.edad = edad;
  }

  saludarMal(){
    setTimeout(function(){
      console.log("Hola "+ this.nombre);
    },3000)
  }

  saludarBien(){
    setTimeout(()=>{
      console.log("Hola "+ this.nombre);
    },3000)
  }

  saludar(){
    var este=new Persona();
    Object.assign(este,this);
    setTimeout(function(){
      console.log("Hola "+ este.nombre);
    },3000)
  }

}

const p1 = new Persona("Luciana",35);
p1.saludarBien();
p1.saludarMal();
p1.saludar();
p1.nombre="Juan";
