

const db = [
  {id:0,nombre:"Juan"},
  {id:1,nombre:"Pedro"},
  {id:3,nombre:"Rocio"}
];


const buscar4Id= (id)=>{
  return new Promise((resolver,rechazar)=>{

    setTimeout(()=>{
      const r=db.find((e)=>e.id===id)
      if (r){
        resolver(r);
      }
      else {
        rechazar(`no existe ${id}`);
      }
    },10);

  })



}

const buscarAndThen=(id)=>buscar4Id(id).then((res)=>console.log(res)).catch((err)=>console.error(err));
buscarAndThen(1);
buscarAndThen(2);
console.log("ok");




const buscarAsinc = async (id) =>{
  try{
    console.log(await buscar4Id(id));
  } catch(error){
    console.error(error);
  }
};


buscarAsinc(3);
buscarAsinc(4);

console.log();
